package main

import(
	"fmt"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

func (f Flags) scrapeMultiplePages() (results []Person, err error) {
	if len(f.Uri) != 2 {
		err = errors.New("A split uri should consist of 2 parts")
		return
	}
	var result []Person
	for i := 0; i <= f.Pages; i++ {
		log.Println("Starting to scrape page " + strconv.Itoa(i))
		result, err = f.scrapePage(f.Uri[0] + strconv.Itoa(i) + f.Uri[1])
		if err != nil {
			return
		}
		results = append(results, result...)
		if f.Test {
			return
		}
	}
	return
}

func (f Flags) scrapePage(uri string) (persons []Person, err error) {
	lines, err := getHtml(uri)
	if err != nil {
		return
	}
	mailOnPage := mailOnPage(lines)
	var linkOn bool
	var person Person
	for _, line := range lines {
		if f.Type != "" {
			person.Type = f.Type
		}
		bits := f.extractInfo(line)
		if len(bits) > 0 {
			for _, bit := range bits {
				linkOn = person.addNameOrType(bit, f.Type != "", mailOnPage, f)
			}
		}
		linkOn, err = person.addMail(line, f, linkOn)
		if err != nil {
			return
		}
		if (len(person.Name) > 0) && (len(person.Mail) > 0) && (len(person.Type) > 0) {
			fmt.Printf("Adding: %v\n", person)
			persons = append(persons, person)
			if f.Test {
				return
			}
			person = Person{}
		}
	}
	return
}

func getHtml(uri string) (lines []string, err error) {
	resp, err := http.Get(uri)
	if err != nil {
		return
	}
	if resp.Status != "200 OK" {
		err = errors.New("Returned " + resp.Status + " for request to " + uri)
		return
	}
	defer resp.Body.Close()
	html, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	lines = strings.Split(string(html), "\n")
	return
}

func (f Flags) extractInfo(line string) (infos []string) {
	bits := strings.Split(line, ">")
	for _, bit := range bits {
		bit = strings.TrimSpace(bit)
		i := strings.Index(bit, "<")
		if i > 0 {
			infos = append(infos, strings.ReplaceAll(bit[:i], "\"", ""))
		}
	}
	return
}

func cleanUri(uri string) (clean string) {
	clean = strings.Split(uri, "\"")[0]
	return
}

func getMail(bit string) (mail string) {
	regex := regexp.MustCompile(`mailto:.+@.+"`)
	mail = regex.FindString(bit)
	mail = strings.ReplaceAll(mail, "mailto:", "")
	mail = strings.Split(mail, "\"")[0]
	return
}

func (f Flags) useFilter(bit string) bool {
	for _, filter := range f.Filter {
		if strings.Contains(strings.ToLower(bit), filter) {
			fmt.Println("Using filter " + filter + " on: " + bit)
			return true
		}
	}
	return false
}

func mailOnPage(lines []string) bool {
	i := 0
	for _, line := range lines {
		if strings.Contains(line, "mailto:") {
			i++
		}
	}
	return i > 3
}

func (p *Person) addNameOrType(bit string, typeOn bool, mailOnPage bool, flags Flags) (linkOn bool) {
	if !typeOn && checkIfType(bit) {
		p.Type = bit
	}	else if len(strings.Split(bit, " ")) > 1 && !checkIfType(bit) && !checkIfMail(bit) && flags.checkIfName(bit) {
		p.Name = bit
		if !mailOnPage {
			linkOn = true
		}
	} else {
		fmt.Println(bit + " will not be considered.")
	}
	return
}

func (p *Person) addMail(line string, flags Flags, linkOn bool) (linkOff bool, err error) {
	if strings.Contains(line, "mailto:") {
		bit := getMail(line)
		if !flags.useFilter(bit) {
			p.Mail = bit
		}
	}
	if linkOn && strings.Count(line, "https://") == 1 {
		uriReg := regexp.MustCompile(`http.://.+"`)
		uriProfile := strings.Split(uriReg.FindString(line), "\"")[0]
		log.Println("Following link:\n" + uriProfile)
		var gines []string
		gines, err = getHtml(uriProfile)
		if err != nil {
			return
		}
		for _, gine := range gines {
			if strings.Contains(gine, "mailto:") {
				bit := getMail(gine)
				if !flags.useFilter(bit) {
					p.Mail = bit
				}
			}
		}
		linkOff = false
	}
	return
}
