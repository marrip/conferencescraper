package main

type Flags struct {
	Filename string
	Filter []string
	Pages int
	Type string
	SecTags []string
	Tags []string
	Test bool
	Uri []string
}

type Person struct {
	Name string
	Mail string
	Type string
}
