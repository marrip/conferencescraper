package main

import(
	"flag"
	"strings"
)

func readFlags() (flags Flags, err error) {
	filename := flag.String("filename", "output.csv", "filename for scraped results")
	pages := flag.Int("pages", 0, "how many pages to be scraped")
	typ := flag.String("type", "", "give predefined type")
	test := flag.Bool("test", false, "just show results of first scraped entry and do not print to file.")
	uri := flag.String("uri", "", "give full uri or split uri with comma where page number if several pages of entries to be scraped")
	flag.Parse()
	flags = Flags{
		Filename: *filename,
		Pages: *pages,
		Test: *test,
		Type: *typ,
		Uri: strings.Split(*uri, ","),
	}
	err = flags.loadNameFilters()
	return
}
