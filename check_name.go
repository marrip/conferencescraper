package main

import(
	"encoding/csv"
	"log"
	"os"
	"strings"
)

func (f Flags) checkIfName(name string) bool {
	isName := true
	for _, filter := range f.Filter {
		if strings.Contains(strings.ToLower(name), filter) {
			isName = false
		}
	}
	return isName
}

func (f *Flags) loadNameFilters() (err error) {
	file, err := os.Open("name_filter.csv")
	if err != nil {
		return
	}
	defer file.Close()
	reader := csv.NewReader(file)
	records, err := reader.ReadAll()
	if err != nil {
		return
	}
	for _, record := range records {
		f.Filter = append(f.Filter, record...)
	}
	log.Println("Active filters:\n" + strings.Join(f.Filter, ", "))
	return
}
