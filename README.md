# ConferenceScraper

scrape some scientists for your conference

## Usage

Just run (if compiled):

```
conferencescraper --help
```

or (if not compiled):

```
go run . --help
```

to see the options:

```
  -filename string
    	filename for scraped results (default "output.csv")
  -pages int
    	how many pages to be scraped
  -test
    	just show results of first scraped entry and do not print to file.
  -type string
    	give predefined type
  -uri string
    	give full uri or split uri with comma where page number if several pages of entries to be scraped
```

## Example

If you are scraping a single page list with all information present you may use something like:

```
./conferencescraper -filename='PostDocs_UniX.csv' -uri='https:/unix.edu/postdocs'
```

The scraper will check the page `https://unix.edu/postdocs` for names, mail adresses and types (PostDoc, PhD student, etc.). If the list is split
into several pages, split the uri at the page number and add the number of pages as a flag.

```
./conferencescraper -filename='PostDocs_UniX.csv' -uri='https:/unix.edu/postdocs?page=','&layout=plain' -pages=3
```

For testing the scraper, just append the flag *test* and you will get the first entry as an example. If you need to preset the type to PostDoc,
do so by adding *type* and the type name as a flag:

```
./conferencescraper -filename='PostDocs_UniX.csv' -uri='https:/unix.edu/postdocs' -type='PostDoc'
```

Make sure the working directory contains the file name_filter.csv which contains a list of filters for the name field. Add filters to the file if
necessary. The verbosity of the scraper helps you to identify which filters are appropriate and which filters are to stringent.

If certain types are not recognized by the scraper, contact the developer. I am happy to add anything required.

**HAPPY SCRAPING**
