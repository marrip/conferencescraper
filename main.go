package main

import(
	"fmt"
	"log"
)

func main() {
	printVersion("2.0.0")
	flags, err := readFlags()
	if err != nil {
		log.Fatalf("%v", err)
	}
	var results []Person
	if len(flags.Uri) == 0 {
		log.Fatalf("No uri was specified. Aborting.")
	} else if len(flags.Uri) == 1 {
		log.Println("Starting to scrape")
		results, err = flags.scrapePage(flags.Uri[0])
		if err != nil {
			log.Fatalf("%v", err)
		}
	} else {
		results, err = flags.scrapeMultiplePages()
		if err != nil {
			log.Fatalf("%v", err)
		}
	}
	if flags.Test {
			log.Printf("Example: %v", results)
			log.Println("Just testing. Stopping now.")
			return
	}
	err = flags.writeCsv(results)
	if err != nil {
		log.Fatalf("%v", err)
	}
	return
}

func printVersion(version string) {
	fmt.Println("\n---------------------------------------")
	fmt.Println("\nConference scraper - version " + version)
	fmt.Println("\n---------------------------------------")
	fmt.Println()
	return
}
