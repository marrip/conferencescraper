package main

import(
	"strings"
)

func checkIfType(typ string) bool {
	filters := loadTypeFilter()
	for _, filter := range filters {
		if strings.Contains(strings.ToLower(typ), filter) {
			return true
		}
	}
	return false
}

func loadTypeFilter() (filter []string) {
	filter = []string{
		"assistant",
		"associate",
		"candidate",
		"chair",
		"dean",
		"director",
		"fellow",
		"investigator",
		"postdoc",
		"post doctoral",
		"post-doctoral",
		"professor",
		"scientist",
		"specialist",
		"student",
		"visiting",
	}
	return
}
