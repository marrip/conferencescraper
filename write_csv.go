package main

import(
	"encoding/csv"
	"os"
)

func (f Flags) writeCsv(results []Person) (err error) {
	file, err := os.Create(f.Filename)
	if err != nil {
		return
	}
	defer file.Close()
	writer := csv.NewWriter(file)
	defer writer.Flush()
	for _, person := range results {
		err = writer.Write([]string{person.Name, person.Mail, person.Type})
		if err != nil {
			return
		}
	}
	return
}
